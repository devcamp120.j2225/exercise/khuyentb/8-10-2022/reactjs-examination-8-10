import 'bootstrap/dist/css/bootstrap.min.css';
import DataTable from './components/Main';

function App() {
  return (
    <div>
      <DataTable />
    </div>
  );
}

export default App;
