import { Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Button, Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAPIOrders, paginationAction } from "../actions/DataTableAction";
import CreateOrderModal from "./Modal/ModalCreate";
import DeleteOrderModal from "./Modal/ModalDelete";
import EditOrderModal from "./Modal/ModalEdit";

export default function DataTable() {

     // useDispatch, useSelector
     const dispatch = useDispatch();

     // Get state from DataTable Reducers
     const { orders, totalPage, currentPage } = useSelector((reduxData) => reduxData.DataTableReducers);

     // Open Modal (Create, Edit, Delete)
     const [createModal, setCreateModal] = useState(false);
     const [editModal, setEditModal] = useState(false);
     const [deleteModal, setDeleteModal] = useState(false);

     // Refresh Page 
     const [refreshPage, setRefreshPage] = useState(0);


     const [rowData, setRowData] = useState([]);
     const [IDDelete, setIDDelete] = useState("");

     // Component Did Update
     useEffect(() => {
          dispatch(fetchAPIOrders());
     }, [currentPage, refreshPage])

     // Dispatch Pagination Action 
     const handlePageChange = (event, value) => {
          dispatch(paginationAction(value));
     }

     //***Button Handler */
     // On Button Create Order Click to Open Modal Create Order 
     const createOrderButtonClick = () => {
          setCreateModal(true);
     }

     // On Button Edit Click to Open Modal Edit Order 
     const onBtnEditClick = (order) => {
          setEditModal(true);
          setRowData(order);
     }

     // On Button Edit Click to Open Modal Delete Order 
     const onBtnDeleteClick = (order) => {
          setDeleteModal(true);
          setIDDelete(order.id);
     }

     
     const closeCreateModal = () => setCreateModal(false);
     const closeEditModal = () => setEditModal(false);
     const closeDeleteModal = () => setDeleteModal(false);

     return (
          <Container maxWidth>
               <Grid container mt={4}>
                    <Grid item xs={12} sm={12} md={12} lg={12} textAlign='center'>
                         <h3><b>PIZZA 365 ORDER LIST</b></h3>
                    </Grid>
               </Grid>
               <Grid container mt={4}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                         <Button style={{backgroundColor: "black"}} variant="contained" onClick={createOrderButtonClick}>CREATE ORDER</Button>
                    </Grid>
               </Grid>
               <Grid item style={{ width: "100%" }} >
                    <TableContainer component={Paper} >
                         <Table sx={{ minWidth: 650 }} aria-label="simple table">
                              <TableHead>
                                   <TableRow>
                                        <TableCell><b>ID</b></TableCell>
                                        <TableCell align="center"><b>Order Code</b></TableCell>
                                        <TableCell align="center"><b>Pizza Size</b></TableCell>
                                        <TableCell align="center"><b>Pizza Type</b></TableCell>
                                        <TableCell align="center"><b>Drink</b></TableCell>
                                        <TableCell align="center"><b>Cost</b></TableCell>
                                        <TableCell align="center"><b>Customer Name</b></TableCell>
                                        <TableCell align="center"><b>Customer Number</b></TableCell>
                                        <TableCell align="center"><b>Status</b></TableCell>
                                        <TableCell align="center"><b>Action</b></TableCell>
                                   </TableRow>
                              </TableHead>
                              <TableBody>
                                   {orders.map((order, index) => (
                                        <TableRow
                                             key={order.id}
                                             sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                             <TableCell component="th" scope="row">
                                                  {order.id}
                                             </TableCell>
                                             <TableCell align="center">{order.orderCode}</TableCell>
                                             <TableCell align="center">{order.kichCo}</TableCell>
                                             <TableCell align="center">{order.loaiPizza}</TableCell>
                                             <TableCell align="center">{order.idLoaiNuocUong}</TableCell>
                                             <TableCell align="center">{order.thanhTien}</TableCell>
                                             <TableCell align="center">{order.hoTen}</TableCell>
                                             <TableCell align="center">{order.soDienThoai}</TableCell>
                                             <TableCell align="center">{order.trangThai}</TableCell>
                                             <TableCell align="center">
                                                  <Button onClick={onBtnEditClick} value={index} style={{ backgroundColor: "#54BAB9", padding: "10px 20px", fontSize: "10px" }} variant="contained">Edit</Button>
                                                  <Button onClick={onBtnDeleteClick} value={index * index} style={{ backgroundColor: "#FF9494", padding: "10px 20px", fontSize: "10px" }} variant="contained">Delete</Button>
                                             </TableCell>
                                        </TableRow>
                                   ))}
                              </TableBody>
                         </Table>
                    </TableContainer>
               </Grid>
               <Grid container justifyContent='end' className="mt-3 mb-4">
                    <Pagination count={totalPage} defaultPage={currentPage} onChange={handlePageChange} />
               </Grid>
               <CreateOrderModal
                    openProp={createModal} closeProp={closeCreateModal} refreshPageProp={refreshPage} setRefreshPageProp={setRefreshPage} />

               <EditOrderModal
                    openProp={editModal} closeProp={closeEditModal} rowDataProp={rowData} refreshPageProp={refreshPage} setRefreshPageProp={setRefreshPage} />

               <DeleteOrderModal
                    openProp={deleteModal} closeProp={closeDeleteModal} IDDeleteProp={IDDelete} refreshPageProp={refreshPage} setRefreshPageProp={setRefreshPage} />
          </Container>
     )
}