import { Grid, Modal, Box, Typography, FormLabel, TextField, Select, MenuItem, FormControl, Button, InputLabel, Snackbar, Alert } from "@mui/material";
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { comboSize, pizzaType, voucherId, fullName, emailOrder, phone, address, message } from "../../actions/CreateModalActions";
import { fetchAPIDrinks, selectDrink } from "../../actions/DrinksAction";
import ConfirmCreateOrderModal from "./ModalConfirmedOrder";

const style = {
     position: 'absolute',
     top: '50%',
     left: '50%',
     transform: 'translate(-50%, -50%)',
     width: 1000,
     bgcolor: 'background.paper',
     border: '10px solid #000',
     boxShadow: 24,
     p: 4,
};

export default function ModalCreate({ openProp, closeProp, refreshPageProp, setRefreshPageProp }) {

     // useDispatch, useSelector
     const dispatch = useDispatch();

     // Get state from Drink Reducers
     const { drinks, drinkSelected } = useSelector((data) => data.DrinksReducer);

     // Get state from Create Modal Reducers
     const { kichCo, loaiPizza, voucher, hoTen, email, soDienThoai, diaChi, loiNhan } = useSelector((data) => data.CreateModalReducers);

     // Alert
     const [alert, setAlert] = useState(false);
     const [textAlert, setTextAlert] = useState("");
     const [alertColor, setAlertColor] = useState("");

     // Automatic object based on Pizza Type
     const [duongKinh, setDuongKinh] = useState("");
     const [suon, setSuon] = useState("");
     const [salad, setSalad] = useState("");
     const [soLuongNuoc, setSoLuongNuoc] = useState("");
     const [thanhTien, setThanhTien] = useState("");

     // Open Comfirm Modal
     const [confirmCreateOrderModal, setConfirmCreateOrderModal] = useState(false);
     const [orderCode, setOrderCode] = useState("");

     // Components run one time after rendering
     useEffect(() => {
          dispatch(fetchAPIDrinks());
     }, [])

     // Dispatch Input Value
     const drinkChange = (e) => dispatch(selectDrink(e.target.value));
     const pizzaTypeSelectChange = (e) => dispatch(pizzaType(e.target.value));
     const inputNameChange = (e) => dispatch(fullName(e.target.value))
     const inputEmailChange = (e) => dispatch(emailOrder(e.target.value))
     const inputPhoneChange = (e) => dispatch(phone(e.target.value))
     const inputAddressChange = (e) => dispatch(address(e.target.value))
     const inputMessageChange = (e) => dispatch(message(e.target.value))


     const sizeSelectChange = (e) => {
          dispatch(comboSize(e.target.value));
          if (e.target.value === "S") {
               setDuongKinh(20);
               setSuon(2);
               setSalad(200);
               setSoLuongNuoc(2);
               setThanhTien(150000);
          }
          if (e.target.value === "M") {
               setDuongKinh(25);
               setSuon(4);
               setSalad(300);
               setSoLuongNuoc(3);
               setThanhTien(200000);
          }
          if (e.target.value === "L") {
               setDuongKinh(30);
               setSuon(8);
               setSalad(400);
               setSoLuongNuoc(4);
               setThanhTien(250000);
          }
     }


     const inputVoucherChange = (e) => {
          dispatch(voucherId(e.target.value))
          var addOrder = {
               kichCo: kichCo,
               duongKinh: duongKinh,
               suon: suon,
               salad: salad,
               loaiPizza: loaiPizza,
               idVourcher: voucher,
               idLoaiNuocUong: drinkSelected,
               soLuongNuoc: soLuongNuoc,
               hoTen: hoTen,
               thanhTien: thanhTien,
               email: email,
               soDienThoai: soDienThoai,
               diaChi: diaChi,
               loiNhan: loiNhan,
               phanTramGiamGia: 0,
          }
          if (addOrder.idVourcher === "12332" || addOrder.idVourcher === "81433" || addOrder.idVourcher === "95531") {
               fetchAPIVoucher("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + addOrder.idVourcher)
                    .then((data) => {
                         console.log(data)
                         addOrder.phanTramGiamGia = data.discount
                         addOrder.thanhTien = addOrder.thanhTien * (1 - addOrder.phanTramGiamGia / 100)
                         console.log(addOrder.thanhTien)
                         setThanhTien(addOrder.thanhTien)
                         setAlert(true);
                         setTextAlert("Valid Voucher!");
                         setAlertColor("success");
                    })
                    .catch((error) => {
                         setAlert(true);
                         setTextAlert("Invalid Voucher!");
                         setAlertColor("error");
                    })
          }
          else {
               addOrder.phanTramGiamGia = 0;
               addOrder.thanhTien = addOrder.thanhTien * (1 - addOrder.phanTramGiamGia / 100);
               setThanhTien(addOrder.thanhTien)
               setAlert(true);
               setTextAlert("Invalid Voucher!");
               setAlertColor("error");
          };
     }


     // Event Handler 
     const handleCloseAlert = () => setAlert(false);
     const onBtnCancelClick = () => closeProp();
     const closeConfirmCreateOrderModal = () => setConfirmCreateOrderModal(false);

     const fetchAPICreateOrder = async (url, body) => {
          const res = await fetch(url, body);
          const data = await res.json();
          return data;
     }

     const fetchAPIVoucher = async (url, body) => {
          const res = await fetch(url, body);
          const data = await res.json();
          return data;
     }

     const onBtnCreateOrderClick = () => {
          var addOrder = {
               kichCo: kichCo,
               duongKinh: duongKinh,
               suon: suon,
               salad: salad,
               loaiPizza: loaiPizza,
               idVourcher: voucher,
               idLoaiNuocUong: drinkSelected,
               soLuongNuoc: soLuongNuoc,
               hoTen: hoTen,
               thanhTien: thanhTien,
               email: email,
               soDienThoai: soDienThoai,
               diaChi: diaChi,
               loiNhan: loiNhan,
               phanTramGiamGia: 0,
          }
          var validateData = validateNewOrderData(addOrder);
          if (validateData) {
               const body = {
                    method: 'POST',
                    body: JSON.stringify({
                         kichCo: addOrder.kichCo,
                         duongKinh: addOrder.duongKinh,
                         suon: addOrder.suon,
                         salad: addOrder.salad,
                         loaiPizza: addOrder.loaiPizza,
                         idVourcher: addOrder.idVourcher,
                         idLoaiNuocUong: addOrder.idLoaiNuocUong,
                         soLuongNuoc: addOrder.soLuongNuoc,
                         hoTen: addOrder.hoTen,
                         thanhTien: addOrder.thanhTien,
                         email: addOrder.email,
                         soDienThoai: addOrder.soDienThoai,
                         diaChi: addOrder.diaChi,
                         loiNhan: addOrder.loiNhan,
                    }),
                    headers: {
                         'Content-type': 'application/json; charset=UTF-8'
                    }
               }
               fetchAPICreateOrder("http://203.171.20.210:8080/devcamp-pizza365/orders", body)
                    .then((data) => {
                         console.log(data)
                         closeProp();
                         setConfirmCreateOrderModal(true);
                         setAlert(true);
                         setTextAlert("Create New Order successfully!");
                         setAlertColor("success");
                         setOrderCode(data.orderCode);
                    })
                    .catch((err) => {
                         setAlert(true);
                         setTextAlert("False to create New Order, please try again!");
                         setAlertColor("error");
                    })
          }
     }

     const validateNewOrderData = (addOrder) => {
          if (addOrder.kichCo === "") {
               setAlert(true);
               setTextAlert("Please select Combo Type !");
               setAlertColor("error");
               return false;
          }

          if (addOrder.loaiPizza === "") {
               setAlert(true);
               setTextAlert("Please select Pizza Type !");
               setAlertColor("error");
               return false;
          }

          if (addOrder.idLoaiNuocUong === "") {
               setAlert(true);
               setTextAlert("Please select your Drinks !");
               setAlertColor("error");
               return false;
          }

          if (addOrder.hoTen === "") {
               setAlert(true);
               setTextAlert("Please input your Name !");
               setAlertColor("error");
               return false;
          }

          if (addOrder.email === "") {
               setAlert(true);
               setTextAlert("Please input your Email !");
               setAlertColor("error");
               return false;
          }

          var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
          if (!vRegexStr.test(addOrder.email)) {
               setAlert(true);
               setTextAlert("Your Email is invalid !");
               setAlertColor("error");
               return false;
          }

          if (addOrder.soDienThoai === "") {
               setAlert(true);
               setTextAlert("Please input your phone num !");
               setAlertColor("error");
               return false;
          }

          if (addOrder.diaChi === "") {
               setAlert(true);
               setTextAlert("Please input your address !");
               setAlertColor("error");
               return false;
          }
          return true;

     }

     return (
          <Container>
               <Grid container>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                         <Modal open={openProp}>
                              <Box sx={style}>
                                   <Typography variant="h5" component="h2" textAlign={'center'}>
                                        <b>CREATE NEW ORDER</b>
                                   </Typography>
                                   <Grid container mt={2}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Size</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <FormControl fullWidth>
                                                  <InputLabel>-- Please select Combo Type --</InputLabel>
                                                  <Select
                                                       value={kichCo}
                                                       label="Kích Cỡ"
                                                       onChange={sizeSelectChange}
                                                  >
                                                       <MenuItem value={'S'}>Small</MenuItem>
                                                       <MenuItem value={'M'}>Medium</MenuItem>
                                                       <MenuItem value={'L'}>Large</MenuItem>
                                                  </Select>
                                             </FormControl>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1} pl={2}>
                                             <FormLabel>Diameter</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Đường Kính" value={duongKinh} onChange={sizeSelectChange} disabled></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={1}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Baked ribs</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Sườn Nướng" value={suon} onChange={sizeSelectChange} disabled></TextField>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pl={2} pt={1}>
                                             <FormLabel>Salad</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Salad" value={salad} onChange={sizeSelectChange} disabled></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={1}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Drinks</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Drinks" value={soLuongNuoc} onChange={sizeSelectChange} disabled></TextField>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pl={2} pt={1}>
                                             <FormLabel>Total Cost</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Total Cost" value={thanhTien} onChange={sizeSelectChange} disabled></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={1}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Pizza Type</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <FormControl fullWidth>
                                                  <InputLabel> -- Please select Pizza Type --</InputLabel>
                                                  <Select
                                                       value={loaiPizza}
                                                       label="Pizza Type"
                                                       onChange={pizzaTypeSelectChange}
                                                  >
                                                       <MenuItem value={'Seafood'}>Seafood</MenuItem>
                                                       <MenuItem value={'Hawaii'}>Hawaii</MenuItem>
                                                       <MenuItem value={'Bacon'}>Bacon</MenuItem>
                                                  </Select>
                                             </FormControl>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pl={2} pt={1}>
                                             <FormLabel>Voucher</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Voucher" onChange={inputVoucherChange} value={voucher}></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={1}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Drink Types</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <FormControl fullWidth>
                                                  <InputLabel>-- Please choose your drinks --</InputLabel>
                                                  <Select
                                                       value={drinkSelected}
                                                       label="Drinks"
                                                       onChange={drinkChange}
                                                  >
                                                       {drinks.map((drink, index) => {
                                                            return <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                                       })}
                                                  </Select>
                                             </FormControl>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pl={2} pt={1}>
                                             <FormLabel>Name</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Name" onChange={inputNameChange} value={hoTen}></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={1}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Email</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Email" onChange={inputEmailChange} value={email}></TextField>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pl={2} pt={1}>
                                             <FormLabel>Phone Num</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Phone Number" onChange={inputPhoneChange} value={soDienThoai}></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={1}>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pt={1}>
                                             <FormLabel>Địa Chỉ</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField fullWidth variant="outlined" label="Address" onChange={inputAddressChange} value={diaChi}></TextField>
                                        </Grid>
                                        <Grid item xs={2} sm={2} md={2} lg={2} pl={2} pt={1}>
                                             <FormLabel>Address</FormLabel>
                                        </Grid>
                                        <Grid item xs={4} sm={4} md={4} lg={4}>
                                             <TextField
                                                  fullWidth variant="outlined"
                                                  label="Message"
                                                  onChange={inputMessageChange}
                                                  value={loiNhan}></TextField>
                                        </Grid>
                                   </Grid>
                                   <Grid container mt={2}>
                                        <Grid item xs={12} textAlign="end">
                                             <Button color="success" variant="contained" className="me-1" onClick={onBtnCreateOrderClick}>CREATE ORDER</Button>
                                             <Button color="error" variant="contained" onClick={onBtnCancelClick}>CANCEL</Button>
                                        </Grid>
                                   </Grid>
                              </Box>
                         </Modal>
                         <Snackbar
                              open={alert}
                              autoHideDuration={5000}
                              onClose={handleCloseAlert}
                         >
                              <Alert onClose={handleCloseAlert} severity={alertColor}>{textAlert}</Alert>
                         </Snackbar>
                    </Grid>
                    <ConfirmCreateOrderModal
                         openConfirmProp={confirmCreateOrderModal}
                         closeConfirmProp={closeConfirmCreateOrderModal}
                         orderCodeProp={orderCode}
                         refreshPageProp={refreshPageProp}
                         setRefreshPageProp={setRefreshPageProp} />
               </Grid>
          </Container>

     )
}